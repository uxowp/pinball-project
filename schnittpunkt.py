def find_intersection_point(line1, line2, tolerance=1e-6):
    x1, y1, x2, y2 = line1
    x3, y3, x4, y4 = line2

    denominator = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)

    if abs(denominator) < tolerance:
        return None  # Linien sind parallel oder identisch

    intersection_x = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / denominator
    intersection_y = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / denominator

    print(min(x1, x2),)
    print(max(x1, x2),)  

    print(min(y1, y2),)
    print(max(y1, y2),)
    print(min(x3, x4),)
    print(max(x3, x4),)
    print(min(y3, y4),)
    print(max(y3, y4),)
          
    if (min(x1, x2) - tolerance <= intersection_x <= max(x1, x2) + tolerance and
            min(y1, y2) - tolerance <= intersection_y <= max(y1, y2) + tolerance and
            min(x3, x4) - tolerance <= intersection_x <= max(x3, x4) + tolerance and
            min(y3, y4) - tolerance <= intersection_y <= max(y3, y4) + tolerance):
        return intersection_x, intersection_y
    else:
        return None  # Schnittpunkt liegt nicht auf beiden Linien



# Beispielwerte für die begrenzten Linien (x1, y1, x2, y2) und (x3, y3, x4, y4)
line1 = (100, 900, 500, 900)
line2 = (300, 890.6, 300, 903.3)

intersection = find_intersection_point(line1, line2)

if intersection:
    print("Schnittpunkt der begrenzten Linien:", intersection)
else:
    print("Die begrenzten Linien schneiden sich nicht oder liegen parallel.")
