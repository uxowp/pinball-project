import pygame
import sys
import tkinter
import time
from pygame.locals import *
import pinball_game


pygame.init()

#constants
FPS = 60
width = 560
height = 1000

#colors
white = (255,255,255)
black = (0,0,0)
gray = (200,200,200)
blue = (0,0,255)


#Display
screen = pygame.display.set_mode((width, height))
pygame.display.set_caption("Menu")

#Fonts
font_headline = pygame.font.Font(None, 36)
font_button = pygame.font.Font (None, 24)

#Create buttons
button_width = 200
button_height = 50
button_padding = 20
buttons_menu = [
    pygame.Rect((width - button_width) // 2, 200 + i * (button_height + button_padding), button_width, button_height)
    for i in range(4)
]

buttons_s = [
    pygame.Rect((width - button_width) // 2, 200 + i * (button_height + button_padding), button_width, button_height)
    for i in range(3)
]


#Texts for buttons
button_menu = ["Singleplayer", "Multiplayer", "Highscore", "Quit"]
button_s = ["Stage 1", "Stage 2", "Back"]

#Main Game Loop
clock = pygame.time.Clock()
running = True
current_screen ="menu"
while running:
    screen.fill(white)


    if current_screen == "menu":
        #Draw headline
        headline_text = font_headline.render("Game Menu", True, black)
        headline_rect = headline_text.get_rect(center = (width // 2, 50))
        screen.blit(headline_text, headline_rect)

        #Draw buttons
        for i, button in enumerate(buttons_menu):
            pygame.draw.rect(screen, gray, button)
            text = font_button.render(button_menu[i], True, black)
            text_rect = text.get_rect(center = button.center)
            screen.blit(text, text_rect)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1: # Left Mousebutton
                    mouse_pos = pygame.mouse.get_pos() #Get mouse position
                    for i, button in enumerate(buttons_menu):
                        if button.collidepoint(mouse_pos):
                            print(f"Button {button_menu[i]} clicked")
                            if button_menu[i] == "Quit":
                                running = False
                            elif button_menu[i] == "Singleplayer":
                                current_screen = "singleplayer"  # Switch to singleplayer screen
                            elif button_menu[i] == "Multiplayer":
                                current_screen = "multiplayer"  # Switch to multiplayer screen
                            elif button_menu[i] == "Highscores":
                                current_screen = "highscores"  # Switch to highscores screen



    if current_screen == "singleplayer":
        #Draw headline
        headline_text = font_headline.render("Stages", True, black)
        headline_rect = headline_text.get_rect(center = (width // 2, 50))
        screen.blit(headline_text, headline_rect)

        #Draw buttons
        for i, button in enumerate(buttons_s):
            pygame.draw.rect(screen, gray, button)
            text = font_button.render(button_s[i], True, black)
            text_rect = text.get_rect(center = button.center)
            screen.blit(text, text_rect)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1: # Left Mousebutton
                    mouse_pos = pygame.mouse.get_pos() #Get mouse position
                    for i, button in enumerate(buttons_s):
                        if button.collidepoint(mouse_pos):
                            print(f"Button {button_s[i]} clicked")
                            if button_s[i] == "Back":
                                current_screen = "menu"
                            elif button_s[i] == "Stage 1":
                                pinball_game.stage_1()
                            elif button_s[i] == "Stage 2":
                                earlier_screen = "singleplayer"
                                current_screen = "error"

                                
                                                        


    if current_screen =="multiplayer":
        pass

    if current_screen == "highscore":
        pass

    if current_screen == "error":
        #Draw headline
        headline_text = font_headline.render("Not yet available", True, black)
        headline_rect = headline_text.get_rect(center = (width // 2, 50))
        screen.blit(headline_text, headline_rect)

        #Draw buttons
        pygame.draw.rect(screen, gray, button)
        text = font_button.render("Back", True, black)
        text_rect = text.get_rect(center = button.center)
        screen.blit(text, text_rect)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1: # Left Mousebutton
                    mouse_pos = pygame.mouse.get_pos() #Get mouse position
                    if button.collidepoint(mouse_pos):
                            print(f"Button back clicked")
                            current_screen = earlier_screen
                    


    pygame.display.flip()
    clock.tick(FPS)

pygame.quit()
sys.exit()